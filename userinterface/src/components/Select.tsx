import React, { Children, FC } from 'react';


interface Choice {
    label: string,
    value: any
}

interface ISelectProps {
    choices: Choice[],
    onChange: any,
    value: any
}

export const Select: FC<ISelectProps> = ({choices,onChange,value}) => {    
    return <select onChange={(event)=>onChange(event.target.value)} value={value}>
        {choices.map((choice:Choice)=><option key={choice.label}  value={choice.value}>{choice.label}</option>)}
    </select>    
}