import React from 'react'
import Style from '../styles/Layout.module.sass';

export const Menu = (props: any) => {

  return <div className={Style.menu}>Menu|  
  <a href="#/collaborate">Collaborate</a>|
  </div>
}

export const Layout = (props: any) => {
  return <div className={Style.layoutContainer}>
      <div className={Style.header}>Header |         
      </div>
      <Menu/>   
      {props.children}
      <div className={Style.footer}>Footer</div>
    </div>
}
