import React, { FC, useEffect, useState } from 'react';
import Style from '../styles/Page.module.sass';
import ForceGraph3D from 'react-force-graph-3d' //https://github.com/vasturiano/react-force-graph
import ForceGraph2D from 'react-force-graph-2d'
import {v4 as uuid} from 'uuid'
import SpriteText from 'three-spritetext'
import { FOCUS_ALL, FOCUS_ONE, FOCUS_TWO , FOCUS_CHOICES} from '../../../application/src/domain';
import {Select} from '../components/Select'
import {Operation,Graph,Node,Link} from '../../../application/src/types'
import {GraphOperation,newGraph,createRemoveLink, createAddLink, createAddNode,createRemoveNode,createUpdateNode,createUpdateLink}from '../../../application/src/service'
import {Log} from '../../../application/src/persistentlog'

const setNodeDistance = (node: Node, data:Graph) => {    
    const visited:any= {}
    const findRelatedNodes = (node:Node,distance:number)=> {        
        visited[node.id]= true
        node.nodeDistance = distance
        console.log(`${node.id}: ${distance}`)
        const relatedNodes : any = data.links.map((link:any)=>{
            let related = null
            if(link.source.id===node.id) {
                if(!visited[link.target.id]){
                    related = data.nodes.find((n:Node)=>n.id===link.target.id)
                }
            }else if (link.target.id===node.id){
                if(!visited[link.source.id]){
                    related = data.nodes.find((n:Node)=>n.id===link.source.id)
                }
            }
            return related
        }).filter(n=>n)
        for(let i=0;i<relatedNodes.length;i++){            
            const rn = relatedNodes[i]            
            findRelatedNodes(rn,distance+1)
        }
    }
    findRelatedNodes(node,0)    
}

const graphOperation = GraphOperation()
const initalGraph = newGraph()
graphOperation.batchCreate(Log,initalGraph)

const Collaborate: FC = () => {
    
    const [log,setLog] = useState<Operation[]>([...Log])
    const [ data,setData] = useState<Graph>(initalGraph)

    const [ selected,setSelected] = useState<Node|null>(null)
    const [ selectedLink,setSelectedLink] = useState<Node|null>(null)

    const [ view, setView]  = useState<string>('3d')
    const [ nodeViewText, setNodeViewText]  = useState<string>('sphere')

    const [name,setName]= useState<string>('')

    const [focus,setFocus] = useState<number>(FOCUS_ALL)


    const commitData = (data:any)=>{
        setData({
            id: data.id,
            nodes: data.nodes,
            links: data.links
        })
    }

    const persistData = () => {              
        window.localStorage.setItem('Graph',JSON.stringify(log))
    }

    const loadData = () => {
        let g = window.localStorage.getItem('Graph')
        if(g){
            let restoredLog = JSON.parse(g)
            setLog(restoredLog)
            graphOperation.batchCreate(restoredLog,data)
            commitData(data)
        }
    }

    const onNodeHover= (data:any)=>{
    }

    const onLinkClick= (link:any, event:MouseEvent)=>{        
        if(event.ctrlKey){
            const op = createRemoveLink(link.id)
            const newLog = [...log,op]
            setLog(newLog)
            graphOperation.batchCreate([op],data)
            commitData(data)
        }else {
            setName(link.name)            
            setSelected(null)
            setSelectedLink(link)            
        }
    }

    const onNodeClick= (node:any, event:MouseEvent)=>{
        if(event.ctrlKey && !selected){
            const op = createRemoveNode(node.id)
            const newLog = [...log,op]
            setLog(newLog)
            graphOperation.batchCreate([op],data)
            commitData(data)
        }else if(event.ctrlKey && selected){
            const op = createAddLink({source: selected.id,target: node.id})
                const newLog = [...log,op]
                setLog(newLog)
                graphOperation.batchCreate([op],data)
                commitData(data)
                setSelected(null)
                setName('')
        }else {
            setName(node.name)            
            setSelected(node)
            setSelectedLink(null)
            setNodeDistance(node,data)
        }
    }

    const onBackgroundClick= (event:MouseEvent)=>{        
        if(selected && event.ctrlKey) {
            const newID = uuid()
            const op = createAddNode({id: newID,name: newID,size: 1})
            const op2 = createAddLink({source: newID, target: selected.id})
            const newLog = [...log,op,op2]
            setLog(newLog)
            graphOperation.batchCreate([op,op2],data)
            commitData(data)
            setSelected(null)
            setName('')
        }
    }

    const onViewChange=()=>{
        setView(view==='2d'? '3d': '2d')
    }
    const onNodeViewChange=()=>{
        setNodeViewText(nodeViewText === 'sphere'? 'text': 'sphere')
    }

    const nodeVis = (node:any)=> {
        return node.nodeDistance <= focus
    }
    const linkVis = (link:any)=> {
        let s = link.source
        if(typeof link.source ==='string' )
            s = data.nodes.find((n:any)=>{                
                return n.id === link.source
            })
        let t = link.target
        if(typeof link.target ==='string' )
            t = data.nodes.find((n:any)=>n.id === link.target)
        return s.nodeDistance <= focus && t.nodeDistance <=focus
    }

    const nodeView3d = nodeViewText==='text' ? (node:any) => {
            const sprite = new SpriteText(node.name);
            sprite.color = node.color;
            sprite.textHeight = 8;
            return sprite;
    } : undefined 


    const nodeView2d = nodeViewText==='text' ? (node:any, ctx:any, globalScale:any) => {
        if(node.x && node.y){
            const label = node.name;
            const fontSize = 12/globalScale;
            ctx.font = `${fontSize}px Sans-Serif`;
            const textWidth = ctx.measureText(label).width;
            const bckgDimensions = [textWidth, fontSize].map(n => n + fontSize * 0.2); // some padding

            ctx.fillStyle = 'rgba(255, 255, 255, 0.8)';
            ctx.fillRect(node.x - bckgDimensions[0] / 2, node.y - bckgDimensions[1] / 2, ...bckgDimensions);

            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.fillStyle = node.color ? node.color : 'green';
            ctx.fillText(label, node.x, node.y);
        }
      } : undefined

    const onNameChange=(e:any)=>{        
        setName(e.target.value)
    }

    const onNameSet=(e:any)=>{
        if(e.code==='Enter' && selected){
            const op = createUpdateNode({id: selected.id,name: name})            
            const newLog = [...log,op]
            setLog(newLog)
            graphOperation.batchCreate([op],data)
            commitData(data)
        } else if(e.code==='Enter' && selectedLink){
            const op = createUpdateLink({id: selectedLink.id,name: name})            
            const newLog = [...log,op]
            setLog(newLog)
            graphOperation.batchCreate([op],data)
            commitData(data)
        }
    }

    const classColor = (node:any)=>{        
        let color = `0x${Math.floor(Math.random()*10)}${Math.floor(Math.random()*10)}${Math.floor(Math.random()*10)}${Math.floor(Math.random()*10)}${Math.floor(Math.random()*10)}${Math.floor(Math.random()*10)}${Math.floor(Math.random()*0)}`        
        return color
    }

    const onChangeFocus = (newFocus:any)=>{
        setFocus(newFocus)
    }
    
    return <div className={Style.pageContainer}>
        <button onClick={onViewChange}>{view}</button>
        <button onClick={onNodeViewChange}>{nodeViewText}</button>
        <button onClick={persistData}>Persist Graph</button>
        <button onClick={loadData}>Load Graph</button>
        <input onChange={onNameChange} onKeyDown={onNameSet} value={name}></input>
        <Select onChange={onChangeFocus} choices={FOCUS_CHOICES} value={focus}/>

        {view==='3d' ?
            <ForceGraph3D graphData={data} width={800} height={800} 
                onNodeHover={onNodeHover} 
                onNodeClick={onNodeClick} 
                onLinkClick={onLinkClick}
                backgroundColor="gray"
                nodeThreeObject={nodeView3d}
                nodeAutoColorBy={classColor}
                linkAutoColorBy={"green"}
                nodeVisibility={nodeVis}
                linkVisibility={linkVis}
                //linkDirectionalParticles="value"
                //linkDirectionalParticleSpeed={(d:any) => d.value * 0.0005}
                //linkDirectionalParticleWidth={(d:any) => d.value/5}
                onBackgroundClick={onBackgroundClick}/> :
            <ForceGraph2D graphData={data} width={800} height={800} 
                nodeVisibility={nodeVis}
                linkVisibility={linkVis}
                onNodeHover={onNodeHover} 
                onNodeClick={onNodeClick} 
                onLinkClick={onLinkClick}
                linkLabel="label"
                linkLineDash={[1,0,1]}
                //linkWidth={1}
                //linkVisibility={true}
                linkDirectionalArrowLength={10}
                linkDirectionalArrowRelPos={0.5}
                //linkCurvature={0.25}
                nodeCanvasObject={nodeView2d}
                onBackgroundClick={onBackgroundClick}/>
        }
    </div>
}

export default Collaborate