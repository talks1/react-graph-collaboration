import React, { Suspense } from 'react';
import { HashRouter, Switch, Route } from "react-router-dom"
import { Layout } from './layout'
import Home from './pages/Home.page'
import Collaborate from './pages/Collaborate.page'
import { Loader } from './components'

function App() {
  return <Suspense fallback={<Loader />}>
          <HashRouter hashType='slash'>
            <Layout>
              <Switch>                
                <Route path="/collaborate" component={Collaborate} />
                <Route path="/" exact component={Home} />
              </Switch>    
              </Layout>
            </HashRouter>
          </Suspense>
}

export default App; 