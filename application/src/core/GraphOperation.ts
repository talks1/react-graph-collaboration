import {Graph,IGraphOperation} from '../types'
import {GRAPH_TYPE,NODE_TYPE,LINK_TYPE,ADD_OPERATION,UPDATE_OPERATION,REMOVE_OPERATION} from '../domain'
import { GraphBuilder } from './GraphBuilder'
import {v4 as uuid}  from 'uuid'

export const newGraph : ()=>Graph = ()=> {
    return {
        id: uuid(),
        nodes: [],
        links: []
    }
}

export const createAddNode = (data:any)=>{
    const {id,name,size} = data
    return  {
        type: NODE_TYPE,
        operation: ADD_OPERATION,
        id,name,size
    }
}

export const createRemoveNode = (id:string)=>{
    return {
        type: NODE_TYPE,
        operation: REMOVE_OPERATION,
        id
    }
}

export const createUpdateNode = (data:any)=>{
    const {id,name} = data
    return  {
        type: NODE_TYPE,
        operation: UPDATE_OPERATION,
        id,
        name
    }
}

export const createAddLink = (data:any)=>{
    const {source,target,size} = data
    return {
        type: LINK_TYPE,
        operation: ADD_OPERATION,
        source,
        target,
        id: `${source}-${target}`
    }
}

export const createRemoveLink = (id:string)=>{
    return {
        type: LINK_TYPE,
        operation: REMOVE_OPERATION,
        id
    }
}

export const createUpdateLink = (data:any)=>{
    const {id,name} = data
    return {
        type: LINK_TYPE,
        operation: UPDATE_OPERATION,        
        id,name
    }
}


export const GraphOperation = () : IGraphOperation => {

    const create = (specification:any,data:Graph)=>{        
        if(specification.type===GRAPH_TYPE){
            GraphBuilder(data,specification)
        } else if(specification.type===NODE_TYPE){
            GraphBuilder(data,specification)
        } else if(specification.type===LINK_TYPE){
            GraphBuilder(data,specification)
        }
        return data
    }


    const batchCreate = (log:any,data:Graph) => {        
        for(var i=0;i<log.length;i++){
            try {
                create(log[i],data)
            }catch(ex){
                console.log(ex)
                console.error("Unable to create")
                console.error(log[i])
            }
        }
        return data
    }
    
    return {
        batchCreate,
        create
    } 
}

