
import { UUID } from 'sequelize/types'
import {GRAPH_TYPE,NODE_TYPE,LINK_TYPE, ADD_OPERATION,REMOVE_OPERATION,UPDATE_OPERATION} from '../domain'
import {Graph,Operation,Node,Link} from '../types'
import { v4 as uuid} from 'uuid'
import Debug from 'debug'

const debug = Debug('graph')

export const GraphBuilder =(data:Graph,operation:Operation)=>{
    
    if(operation.type===GRAPH_TYPE){
        debug('New Graph')
        data.nodes = []
        data.links=[]
        data.id = operation.id || uuid()
    }else if (operation.type===NODE_TYPE && operation.operation===ADD_OPERATION){
        debug('ADD node')
        const newNode : Node = {
            id: operation.id || uuid(),
            name: operation.name || 'name',
            nodeDistance: 0,
            class: operation.class,
            size: operation.size ? operation.size: 1
        }
        data.nodes.push(newNode)
    }else if (operation.type===NODE_TYPE && operation.operation===REMOVE_OPERATION){
        debug('REMOVE node')
        const links = data.links.filter((l:Link)=>l.source === operation.id || l.target === operation.id || l.source.id === operation.id || l.target.id === operation.id)
        links.forEach((removeLink:Link)=>{
            let index = data.links.findIndex((l:Link)=>l.id === removeLink.id)
            data.links.splice(index,1)
        })
        
        let index = data.nodes.findIndex((n:Node)=>n.id === operation.id)
        data.nodes.splice(index,1)
    } else if (operation.type===NODE_TYPE && operation.operation===UPDATE_OPERATION){
            debug('UPDATE node')
            const nodeToUpdate = data.nodes.find((n:Node)=>n.id === operation.id)
            if(nodeToUpdate){
                if(operation.name)
                nodeToUpdate.name = operation.name
            }
    }else if (operation.type===LINK_TYPE && operation.operation===ADD_OPERATION){
        debug('ADD link')
        if(operation.source && operation.target){
            const sourceNode = data.nodes.find((n:Node)=>n.id===operation.source)
            const targetNode = data.nodes.find((n:Node)=>n.id===operation.target)

            const newLink : Link = {
                id: `${operation.source}-${operation.target}`,
                name: 'link',
                source: sourceNode? sourceNode : operation.source,
                target: targetNode? targetNode : operation.target,
                size: operation.size ? operation.size: 1
            }
            data.links.push(newLink)
        }
    }else if (operation.type===LINK_TYPE && operation.operation===REMOVE_OPERATION){
        debug('REMOVE link')
        let index = data.links.findIndex((l:Link)=>l.id === operation.id)
        data.links.splice(index,1)
    } else if (operation.type===LINK_TYPE && operation.operation===UPDATE_OPERATION){
        debug('UPDATE link')
        const toUpdate = data.links.find((n:Link)=>n.id === operation.id)
        if(toUpdate){
            if(operation.name)
            toUpdate.name = operation.name
        }
    }
}