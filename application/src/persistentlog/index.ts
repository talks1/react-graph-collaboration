import {Operation} from '../types'
import {createAddNode, createRemoveNode, createUpdateNode, createAddLink,createRemoveLink,createUpdateLink} from '../core/GraphOperation'

import {GRAPH_TYPE,NODE_TYPE,LINK_TYPE, ADD_OPERATION,REMOVE_OPERATION} from '../domain/'

export const ADD_GRAPH = { 
  type: GRAPH_TYPE,
  operation: ADD_OPERATION,
  id: "graph1",
  name: "graph1",
}

export const ADD_NODE1 = createAddNode({id: "id1",name: "id1",size: 10,class: 'someClass'})
export const ADD_NODE2 = createAddNode({id: "id2",name: "id2",size: 5,class: 'someClass'})
export const REMOVE_NODE2= createRemoveNode("id2")
export const ADD_LINK1 = createAddLink({source:"id1",target:"id2",size: 10})
export const REMOVE_LINK1=createRemoveLink("id1-id2")
export const UPDATE_NODE1 = createUpdateNode({id: "id1",name: "updated id1"})
export const UPDATE_LINK1 = createUpdateLink({id: "id1-id2",name: "updated id1-id2"})
    
export const Log = [ 
    ADD_GRAPH,
    ADD_NODE1,
    ADD_NODE2,
    ADD_LINK1,
    UPDATE_NODE1,
    UPDATE_LINK1
  ]