import Debug from 'debug'
import { Sequelize } from 'sequelize'
const debug = Debug('service')
import { GraphFactory }  from './Graph.model'
import { OperationFactory }  from './Operation.model'

export const ModelFactory = async (config:any)=>{
    debug('Model Factory')    
    debug(config)

  const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
    host: config.DBHOST,
    dialect: config.DBDIALECT,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
    dialectOptions: {
      encrypt: true,
    },
    logging: config.logging ? console.log : false,

    // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
    // operatorsAliases: false
    // timezone: 'utc'
  })

  const Graph = await GraphFactory(sequelize, config)
  const Operation = await OperationFactory(sequelize, config)
  
  //Parameter.Parameter.belongsToMany(Component.Component,{ through: ComponentParameter.ComponentParameter })
  //Component.Component.hasMany(ComponentParameter.ComponentParameter)  

  function close () {
    sequelize.close()
  }

  return {
    Graph,
    Operation,
    close,
  }
}


