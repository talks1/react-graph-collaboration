import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('OperationModel')

export const OperationFactory = async (sequelize:any, config:any) => {
  const Operation = sequelize.define('Operations', {    
    graphId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    date:{
      type: DataTypes.DOUBLE,
      allowNull: false,
    },    
    data: {
      type: DataTypes.STRING,
      allowNull: false,
    },    
  },
  {
    schema: config.schema,
    tableName: 'Operations',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await Operation.sync({ force: config.force }) }

  async function create (opdata:any,graphId: string) {
    let d = {      
      graphId,
      data: JSON.stringify(opdata),
      date: Date.now()
    }    
    return Operation.create(d)
  }

  async function update (data:any) {
    return data.save()
  }

  async function get (id = '') {
    const where = { id }
    return Operation.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await Operation.count({ where })
    return result
  }

  async function query (where = {}) {
    debug('QUery')
    return Operation.findAll({where, include: [{ all: true, nested: true }]})
  }

  async function destroy (id = '') {
    return Operation.destroy({ where: { id } })
  }

  return {
    Operation,
    create,
    update,
    get,
    query,
    queryCount,
    destroy,
  }
}
