import Debug from 'debug'
import { DataTypes  } from 'sequelize'

const debug = Debug('GraphModel')

export const GraphFactory = async (sequelize:any, config:any) => {
  const Graph = sequelize.define('Graphs', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    name:{
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    schema: config.schema,
    tableName: 'Graphs',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await Graph.sync({ force: config.force }) }

  async function create (data:any) {    
      return Graph.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function get (id = '') {
    const where = { id }
    return Graph.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await Graph.count({ where })
    return result
  }

  async function query (where = {}) {
    debug('QUery')
    return Graph.findAll({where, include: [{ all: true, nested: true }]})
  }

  async function destroy (id = '') {
    return Graph.destroy({ where: { id } })
  }

  return {
    Graph,
    create,
    update,
    get,
    query,
    queryCount,
    destroy,
  }
}
