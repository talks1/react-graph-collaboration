import {ModelFactory} from './ModelFactory'
import Debug from 'debug'
import {IGraphModel} from '../types/index'

const debug = Debug('model')

let model :any = null

export const getModel = async (config:any) : Promise<IGraphModel> => {
    if(!model){
        model = {} 
        debug('Building ModelFactory')
        debug(config.database)
        model = await ModelFactory(config.database)
    }
    return model
}

export * from './ModelFactory'