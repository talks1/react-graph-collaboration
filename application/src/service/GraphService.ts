import {IGraphService,IGraphModel} from '../types'
import {GRAPH_TYPE,NODE_TYPE,LINK_TYPE} from '../domain'
import { GraphBuilder } from '../core/GraphBuilder'

export const GraphService = async (model : IGraphModel) : Promise<IGraphService> => {
    
    const data = {
        id: '',
        nodes: [],
        links: []
    }

    const queryGraphs = async ()=>{    
        const relationalModel = await model.Graph.query({})
        return relationalModel
    }

    const getGraph = async (id:string)=>{    
        const relationalModel = await model.Graph.query({id})
        if(relationalModel.length>0)
            return relationalModel[0]
        return null
    }

    const create = async (specification:any)=>{
        if(specification.type===GRAPH_TYPE){
            await model.Graph.create(specification)
            GraphBuilder(data,specification)
        } else if(specification.type===NODE_TYPE){
            await model.Operation.create(specification,data.id)
            GraphBuilder(data,specification)
        } else if(specification.type===LINK_TYPE){            
            await model.Operation.create(specification,data.id)
            GraphBuilder(data,specification)
        }
        return data
    }

    const seed = async (log:any) => {        
        for(var i=0;i<log.length;i++){
            try {
                await create(log[i])
            }catch(ex){
                console.log(ex)
                console.error("Unable to create")
                console.error(log[i])
            }
        }
        return data
    }
    
    return {
        seed,
        create,
        getGraph,
        queryGraphs
    } 
}

