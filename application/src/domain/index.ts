
export const GRAPH_TYPE='Graph'
export const NODE_TYPE='Node'
export const LINK_TYPE='Link'

export const ADD_OPERATION='Add'
export const REMOVE_OPERATION='Remove'
export const UPDATE_OPERATION='Update'

export const FOCUS_ALL=1000
export const FOCUS_ONE=1
export const FOCUS_TWO=2

export const FOCUS_CHOICES: any=[{label:'All',value: FOCUS_ALL,},{label: 'One',value:FOCUS_ONE},,{label: 'Two',value:FOCUS_TWO}]