

export interface IGraphModel {
    Graph: any,
    Operation: any
    close: ()=>void
}

export interface IGraphOperation {
    batchCreate: (log:any,data:Graph)=>Graph,
    create: (spec:any,data:Graph)=>Graph
}

export interface IGraphService {
    seed: (log:any)=>Promise<Graph>,
    create: (spec:any)=>Promise<Graph>,
    getGraph: (id:string)=>Promise<Graph>,
    queryGraphs: ()=>Promise<Graph[]>
}

export interface Operation {
    type: string
    operation: string
    id?: string,
    size?: number,
    name?: string
    value?: string,
    class?: string,
    nodeDistance?: number,
    source?: string,
    target?: string
}

export interface Graph {
    id: string,
    nodes: Node[],
    links: Link[],
}

export interface Node {
    id: string,
    name: string,
    size: number,
    class?: string,
    color?: string,
    x?: number,
    y?: number
    nodeDistance?: number
}

export interface Link {
    id: string,
    name: string,
    source: any,
    target: any,
    size: number,
}

