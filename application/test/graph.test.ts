
import { describe,} from 'riteway';
import {GraphOperation, newGraph} from '../src/service'
import {Graph} from '../src/types'
import {ADD_GRAPH,ADD_NODE1,ADD_NODE2,ADD_LINK1,REMOVE_LINK1,REMOVE_NODE2} from '../src/persistentlog'

describe('Simple Graph', async assert => {  
  try {
    const graphOperation = GraphOperation()

    let theGraph:Graph = newGraph()
  
    graphOperation.batchCreate([ADD_GRAPH,ADD_NODE1],theGraph)
    
    assert({
      given: 'ADD Node',
      should: 'have 1 node',
      actual: theGraph.nodes.length,
      expected: 1
    });

    graphOperation.batchCreate([ADD_NODE2,ADD_LINK1],theGraph)
        
    assert({
      given: 'ADD Node and Link',
      should: 'have 2 nodes and 1 links',
      actual: `${theGraph.nodes.length} ${theGraph.links.length}`,
      expected: '2 1'
    });
    
    graphOperation.batchCreate([REMOVE_NODE2],theGraph)
      
    assert({
      given: 'Remove Node2',
      should: 'have 1 node',
      actual: `${theGraph.nodes.length} ${theGraph.links.length}`,
      expected: '1 0'
    });

    graphOperation.batchCreate([ADD_GRAPH],theGraph)

    assert({
      given: 'Add Graph',
      should: 'have 0 nodes and links',
      actual: `${theGraph.nodes.length} ${theGraph.links.length}`,
      expected: '0 0'
    });
    
  }catch(ex){
    console.log(ex)
  }
  
  assert({
    given: 'no arguments',
    should: 'return 0',
    actual: 0,
    expected: 0
  });

})