const inmemoryConfig = {
    database: {    
      DBDIALECT: 'sqlite',
      force: true,
      RESYNC: 'true',
      logging: false,
    },
    seed: false
  }
  
  const localConfig = {
    database: {    
      USER: "gpc",
      PASSWORD: 'gpc',
      DBHOST: 'localhost',
      DB: 'gpc',
      DBDIALECT: 'mssql',
      schema: 'graph',
      force: true,
      RESYNC: 'true',
      logging: false,
    },
    seed: false
  }
  
  export const config = localConfig

